import Vue from 'vue'

Vue.prototype.$populateSEOMeta = (type, pageTitle, hero, description) => {
  const metaArray = []
  const title = pageTitle || 'I. Welch Canavan'
  const image = hero
    ? hero.src
    : 'https://d28efhqsv6dyl0.cloudfront.net/media/uploads/dc414ea9bd/me-2019.jpg'

  metaArray.push({ property: 'og:type', content: type })
  metaArray.push({ property: 'twitter:card', content: 'summary' })

  metaArray.push({ property: 'og:title', content: title })
  metaArray.push({ property: 'twitter:title', content: title })

  if (description) {
    metaArray.push({ property: 'og:description', content: description })
    metaArray.push({
      property: 'twitter:description',
      content: description
    })
  }

  metaArray.push({ property: 'og:image', content: image })
  metaArray.push({ property: 'twitter:image', content: image })

  return metaArray
}
