const path = require('path')
const express = require('express')
const app = express()

// static pages outside of the app
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '../exterior/dngrs-grf.html'))
})

// export the server middleware
module.exports = {
  path: '/dngrs-grf',
  handler: app
}
