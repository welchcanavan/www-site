require('dotenv').config()
const pkg = require('./package')

module.exports = {
  mode: 'universal',

  server: {
    port: process.env.PORT || 3050
  },

  serverMiddleware: ['redirect-ssl', '~/server/dngrs-grf.js'],

  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000'
  },

  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: pageTitle => {
      const siteTitle = 'I. Welch Canavan'

      return pageTitle ? `${pageTitle} | ${siteTitle}` : siteTitle
    },

    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
      { name: 'msapplication-TileColor', content: '#1458b7' },
      { name: 'theme-color', content: '#1458b7' }
    ],

    link: [
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/apple-touch-icon.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        size: '32x32',
        href: '/favicon-32x32.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        size: '16x16',
        href: '/favicon-16x16.png'
      },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'manifest', href: '/site.webmanifest' },
      { rel: 'mask-icon', href: '/safari-pinned-tab.svg', color: '#1458b7' },
      { rel: 'stylesheet', href: 'https://use.typekit.net/eiu6rpk.css' }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: false,

  /*
   ** Global CSS
   */
  css: ['@/assets/styles/main.scss'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/axios', '~/plugins/utils'],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/google-analytics'
  ],
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: process.env.API_URL
  },
  /*
   ** Analytics module configuration
   */
  googleAnalytics: {
    // SEe https://github.com/nuxt-community/analytics-module
    id: 'UA-41030734-1'
  },

  /*
   ** Build configuration
   */
  build: {
    vendor: ['clipboard', 'reframe.js'],
    /*
     ** You can extend webpack config here
     */
    transpile: ['lodash-es'],
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }

      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
    }
  }
}
