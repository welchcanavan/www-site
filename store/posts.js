import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'

dayjs.extend(relativeTime)

const formatPost = post => {
  return {
    ...post,
    iso8601: dayjs(post.published_at).format('YYYY-MM-DD'),
    relativeDate: dayjs(post.published_at).fromNow()
  }
}

export const state = () => ({
  current: {},
  list: [],
  next: {},
  prev: {}
})

export const mutations = {
  set(state, posts) {
    state.list = posts.map(formatPost)
  },

  setCurrent(state, postSlug) {
    const list = state.list
    const count = list.length
    const currentIndex = list.findIndex(post => post.slug === postSlug)
    const lastIndex = count - 1

    state.current = list[currentIndex]
    state.next = currentIndex === lastIndex ? null : list[lastIndex]
    state.prev = currentIndex === 0 ? null : list[0]
  }
}
