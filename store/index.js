export const actions = {
  async nuxtServerInit({ commit }) {
    const { data } = await this.$axios.get('/posts/')

    await commit('posts/set', data)
  }
}
